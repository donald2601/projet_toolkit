import { Button } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import {useForm} from 'react-hook-form';
import {useState} from 'react'
import { useDispatch } from 'react-redux';
import { addProducts } from '../features/productsSlice';


function Addproduct() {
  
  const dispatch = useDispatch();
  // const [data, setData] = useState();
  const {handleSubmit, register} = useForm ();

  const onSubmit = (dat)=>{
    dispatch(addProducts(dat))
    // setData (dat);
    // console.log(data);
  }
  return (
    <Form onSubmit={handleSubmit(onSubmit)} className='container shadow-lg p-3 d-flex flex-column bg-danger' style={{height:400, width:500 }}>
      <Form.Group className="mb-3" controlId="formGroupEmail">
        <Form.Label> Name </Form.Label>
        <Form.Control type="text" placeholder="name" {...register("name")} />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formGroupPassword">
        <Form.Label> Description </Form.Label>
        <Form.Control type="textarea" placeholder="description" {...register("description")}/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formGroupPassword">
        <Form.Label> Quantite </Form.Label>
        <input type='number' className='form-control' defaultevalue={'1'} {...register("Quantite")}/>
      </Form.Group>
      <Button type='submit' > enregistrer </Button>
    </Form>
  );
}

export default Addproduct;