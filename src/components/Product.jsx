// import RemoveProducts from './RemoveProducts';
import { useDispatch,useSelector } from 'react-redux';
import { removeProducts } from '../features/productsSlice';


function Product() {
    const dispatch = useDispatch();
    const selector = useSelector((state)=> state.product);
    
return (
    <div className='container mt-5 justify-content-between' >
        {selector.map((items)=> {
            return (<div key={items.id} className='container d-flex justify-content-between b-3' style={{width:520, height:100, borderBottom: '4px solid grey'}}>
            <h4 className='m-5'> {items.name} </h4>
            <h4 className='m-5'> {items.description}  </h4>
            <h4 className='m-5'> {items.quantite}  </h4>
            <h4 className='m-5'> <i class="fa-solid fa-trash" onClick={()=> dispatch(removeProducts(items.id))} style={{cursor:'pointer'}}></i> </h4>
        </div>)
        })}
        
    </div>
);
}

export default Product;