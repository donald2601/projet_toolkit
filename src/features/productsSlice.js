import { createSlice } from "@reduxjs/toolkit";


export const productsSlice = createSlice ({
    name : "products",
    initialState : [
            // {id : 1, name : "chausssure", description : "cool", quantite : "1"},
        ],
    reducers : {
        // setProducts : (state,{payload}) => {
        //     state.products = payload ;
        // },

        addProducts : (state ,{payload}) => {
            const newProduct = {
                id : Date.now(),
                name : payload.name,
                description : payload.description,
                quantite : payload.quantite,
            }
            state.push(newProduct);
            return state 
        },

        removeProducts : (state ,{payload}) => {
            state=state.filter((items)=> items.id !== payload)
            return state 
        },
    }
})

export const {addProducts, removeProducts} = productsSlice.actions;
export default productsSlice.reducer;