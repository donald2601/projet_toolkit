import { configureStore } from "@reduxjs/toolkit";
import productsSliceReducer from '../features/productsSlice'

export default configureStore ({
    reducer : {
        product : productsSliceReducer,
    }
});